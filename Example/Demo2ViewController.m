//
//  Demo2ViewController.m
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "Demo2ViewController.h"
#import "KTLimitCharactersTextView.h"
#import "UIGestureRecognizer+HZW.h"

@interface Demo2ViewController ()<KTLimitCharactersTextViewDelegate>

@property (nonatomic, strong) KTLimitCharactersTextView *textView;

@end

@implementation Demo2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.textView];
    
    self.textView.frame = CGRectMake(40, 120, 200, 40);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithActionBlock:^(id sender) {
        [self.textView resignFirstResponder];
    }];
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"%s", __FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"%s", __FUNCTION__);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSLog(@"%s", __FUNCTION__);
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSLog(@"%s", __FUNCTION__);
}

- (KTLimitCharactersTextView *)textView
{
    if (!_textView) {
        _textView = [[KTLimitCharactersTextView alloc] init];
        _textView.limitedCharactersCount = 5;
        _textView.limitDelegate = self;
        _textView.returnKeyType = UIReturnKeyDone;
    }
    
    return _textView;
}

#pragma mark - HZWLimitCharactersTextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSLog(@"textViewDidEndEditing");
}

- (void)textDidChanged:(KTLimitCharactersTextView *)textView
{
    NSLog(@"textView = %@", textView.text);
}

@end
