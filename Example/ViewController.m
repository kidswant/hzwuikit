//
//  ViewController.m
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "ViewController.h"
#import "HZWPagerContentView.h"
#import "Demo1ViewController.h"
#import "Demo2ViewController.h"
#import "Demo3ViewController.h"
#import "HZWPagerFixedTabView.h"
//#import "HZWPagerTabItem.h"
#import "HZWPagerView.h"
#import "UIView+HZWLayout.h"

@interface ViewController ()<HZWPagerContentViewDataSource, HZWPagerContentViewDelegate, HZWPagerViewDataSource, HZWPagerViewDelegate>

@property (nonatomic, strong) HZWPagerContentView *pagerContentView;
@property (nonatomic, strong) HZWPagerFixedTabView *tabView;

@property (nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property (nonatomic, strong) UIBarButtonItem *rightBarButtonItem;

@property (nonatomic, strong) HZWPagerView *pagerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view addSubview:self.pagerView];
    self.pagerView.frame = CGRectOffset(self.view.bounds, 0, 88);
    
    HZWPagerTabItem *item1 = [[HZWPagerTabItem alloc] init];
    item1.title = @"全部订单";
    item1.titleColor = [UIColor blackColor];
    item1.selectedTitleColor = [UIColor redColor];
    
    HZWPagerTabItem *item2 = [[HZWPagerTabItem alloc] init];
    item2.title = @"待支付";
    item2.titleColor = [UIColor blackColor];
    item2.selectedTitleColor = [UIColor redColor];
    
    HZWPagerTabItem *item3 = [[HZWPagerTabItem alloc] init];
    item3.title = @"待发货";
    item3.titleColor = [UIColor blackColor];
    item3.selectedTitleColor = [UIColor redColor];
    
    HZWPagerTabItem *item4 = [[HZWPagerTabItem alloc] init];
    item4.title = @"待收货";
    item4.titleColor = [UIColor blackColor];
    item4.selectedTitleColor = [UIColor redColor];
    
    HZWPagerTabItem *item5 = [[HZWPagerTabItem alloc] init];
    item5.title = @"待评价";
    item5.titleColor = [UIColor blackColor];
    item5.selectedTitleColor = [UIColor redColor];
    
    self.pagerView.items = @[item1, item2, item3, item4, item5];
    
//    [self.view addSubview:self.pagerContentView];
//    self.pagerContentView.frame = CGRectOffset(self.view.bounds, 0, 128);

//    self.tabView.items = @[item1, item2, item3, item4, item5];
//    [self.view addSubview:self.tabView];
//    self.tabView.frame = CGRectMake(0, 88, self.view.bounds.size.width, 40);
    
    self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
    self.navigationItem.rightBarButtonItem = self.rightBarButtonItem;
    
    self.navigationItem.titleView = self.tabView;
    
    self.tabView.height = 44;
    
    self.pagerView.selectedIndex = 1;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - HZWPagerContentViewDataSource
- (NSInteger)numberPagesInHZWPagerContentView:(HZWPagerContentView *)pagerContentView
{
    return 3;
}

- (UIViewController *)hzwPagerContentView:(HZWPagerContentView *)pagerContentView viewControllerForIndex:(NSInteger)index
{
    UIViewController *vc = nil;
    if (index == 0) {
        vc = [[Demo1ViewController alloc] init];
        vc.view.backgroundColor = [UIColor redColor];
        
    }
    else if (index == 1) {
        vc = [[Demo2ViewController alloc] init];
        vc.view.backgroundColor = [UIColor blackColor];
    }
    else {
        vc = [[Demo3ViewController alloc] init];
        vc.view.backgroundColor = [UIColor blueColor];
    }
    return vc;
}

- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex percent:(CGFloat)percent
{
    NSLog(@"percent = %f", percent);
}

- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView didSuccessToNextPage:(NSInteger)toIndex
{
    NSLog(@"didSuccessToNextPage = %@", @(toIndex));
}

- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView didCancelToNextPage:(NSInteger)fromIndex
{
    NSLog(@"didCancelToNextPage = %@", @(fromIndex));
}

#pragma mark - HZWPagerViewDataSource
- (NSInteger)numberOfPagesInHZWPagerView:(HZWPagerView *)pagerView
{
    return 5;
}

- (__kindof UIViewController *)hzwPagerView:(HZWPagerView *)pagerView viewControllerForIndex:(NSInteger)index
{
    UIViewController *vc = nil;
    if (index == 0) {
        vc = [[Demo1ViewController alloc] init];
        vc.view.backgroundColor = [UIColor redColor];
        
    }
    else if (index == 1) {
        vc = [[Demo2ViewController alloc] init];
        vc.view.backgroundColor = [UIColor blackColor];
    }
    else {
        vc = [[Demo3ViewController alloc] init];
        vc.view.backgroundColor = [UIColor blueColor];
    }
    return vc;
}


#pragma mark - HZWPagerViewDelegate

#pragma mark - actions
- (void)previousPage:(id)sender
{
    if (self.pagerContentView.selectedIndex > 0) {
        [self.pagerContentView setSelectedIndex:self.pagerContentView.selectedIndex - 1 animated:NO];
    }
}

- (void)nextPage:(id)sender
{
    if (self.pagerContentView.selectedIndex < 3) {
        [self.pagerContentView setSelectedIndex:self.pagerContentView.selectedIndex + 1 animated:YES];
    }
}

#pragma mark - getters

- (HZWPagerContentView *)pagerContentView
{
    if (!_pagerContentView) {
        _pagerContentView = [[HZWPagerContentView alloc] init];
        _pagerContentView.dataSource = self;
        _pagerContentView.delegate = self;
        _pagerContentView.scrollEnabled = YES;
    }
    
    return _pagerContentView;
}

- (HZWPagerFixedTabView *)tabView
{
    if (!_tabView) {
        HZWPagerTabItem *item4 = [[HZWPagerTabItem alloc] init];
        item4.font = [UIFont systemFontOfSize:15];
        item4.title = @"门店订单";
        item4.titleColor = [UIColor blackColor];
        item4.selectedTitleColor = [UIColor redColor];
        
        HZWPagerTabItem *item5 = [[HZWPagerTabItem alloc] init];
        item5.font = [UIFont systemFontOfSize:15];
        item5.title = @"线上订单";
        item5.titleColor = [UIColor blackColor];
        item5.selectedTitleColor = [UIColor redColor];
        
        _tabView = [[HZWPagerFixedTabView alloc] initWithItems:@[item4, item5] innerMargin:0];
        _tabView.trackColor = [UIColor blackColor];
    }
    
    return _tabView;
}

- (UIBarButtonItem *)leftBarButtonItem
{
    if (!_leftBarButtonItem) {
        _leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"上一页" style:UIBarButtonItemStylePlain target:self action:@selector(previousPage:)];
    }
    return _leftBarButtonItem;
}

- (UIBarButtonItem *)rightBarButtonItem
{
    if (!_rightBarButtonItem) {
        _rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"下一页" style:UIBarButtonItemStylePlain target:self action:@selector(nextPage:)];
    }
    return _rightBarButtonItem;
}

- (HZWPagerView *)pagerView
{
    if (!_pagerView) {
        _pagerView = [[HZWPagerView alloc] init];
        _pagerView.dataSource = self;
        _pagerView.delegate = self;
        _pagerView.trackColor = [UIColor redColor];
    }
    
    return _pagerView;
}


@end
