//
//  KPlaceholderTextView.h
//  KPlaceholderTextView
//
//  Created by ZhangMing on 3/23/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KTPlaceholderTextView : UITextView

@property (nonatomic, strong, nullable) NSString *placeholder;

@property (nonatomic, strong, nullable) UIColor *placeholderColor;

@end
