//
//  HZWPagerContentView.m
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 Leisure. All rights reserved.
//

#import "HZWPagerContentView.h"
#import "UIView+HZW.h"

NS_ASSUME_NONNULL_BEGIN

static const CGFloat kHZWOffsetThreshold = 50.0f;
static const NSTimeInterval kHZWAnimateDuration = .25f;

@interface HZWPagerContentView() {
    NSInteger _totalCount;
}

@property (nonatomic, strong) UIViewController *parentViewController;

/**
 *  当前展示的VC
 */
@property (nonatomic, strong, nullable) UIViewController *currentViewController;
/**
 *  即将展示的VC
 */
@property (nonatomic, strong, nullable) UIViewController *toViewController;
/**
 *  当前展示的VC在数组中的索引
 */
@property (nonatomic, assign) NSInteger currentIndex;
/**
 *  即将展示的VC在数组中的索引
 */
@property (nonatomic, assign) NSInteger toIndex;

/**
 *  滑动手势
 */
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;

/**
 *  滑动手势起始的位置
 */
@property (nonatomic, assign) CGPoint startPanLocation;

@end

@implementation HZWPagerContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commitInit];
    }
    return self;
}

- (void)commitInit
{
    _currentIndex = -1;
    _toIndex = -1;
    [self addGestureRecognizer:self.panGestureRecognizer];
}


#pragma mark - public methods
- (void)changeSelectedIndex:(NSInteger)selectedIndex
{
    
    if (selectedIndex == self.currentIndex) { return; }
    _totalCount = 0;
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberPagesInHZWPagerContentView:)]) {
        _totalCount = [self.dataSource numberPagesInHZWPagerContentView:self];
    }
    
    if (selectedIndex > _totalCount - 1) {  return; }
    self.toIndex = selectedIndex;
    
    // 获取即将展示的VC
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(hzwPagerContentView:viewControllerForIndex:)]) {
        self.toViewController = [self.dataSource hzwPagerContentView:self viewControllerForIndex:self.toIndex];
    }
    
    if (!self.toViewController) { return; }
    if (self.toViewController.parentViewController) { return; }
    
    if (self.currentViewController) {
        [self.currentViewController willMoveToParentViewController:nil];
        [self.currentViewController beginAppearanceTransition:NO animated:NO];
    }
    
    [self.parentViewController addChildViewController:self.toViewController];
    //    [self.toViewController beginAppearanceTransition:YES animated:YES];
    [self.toViewController willMoveToParentViewController:self.parentViewController];
    [self addSubview:self.toViewController.view];
    
    BOOL isNextPage = self.currentIndex < self.toIndex;
    
    if (isNextPage) {
        self.toViewController.view.frame = CGRectMake(self.bounds.size.width, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    }
    else {
        self.toViewController.view.frame = CGRectMake(-self.bounds.size.width, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    }
    
    NSTimeInterval animateDuration = 0.0;
    [UIView animateWithDuration:animateDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        if (isNextPage) {
            self.currentViewController.view.frame = CGRectMake(-CGRectGetWidth(self.bounds), self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        }
        else {
            self.currentViewController.view.frame = CGRectMake(CGRectGetWidth(self.bounds), self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        }
        self.toViewController.view.frame = CGRectMake(0, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    } completion:^(BOOL finished) {
        if (!finished) { return; }
        if (self.currentViewController) {
            [self.currentViewController.view removeFromSuperview];
            [self.currentViewController endAppearanceTransition];
            [self.currentViewController removeFromParentViewController];
        }
        [self.toViewController didMoveToParentViewController:self.parentViewController];
        //        [self.toViewController endAppearanceTransition];
        
        self.currentViewController = self.toViewController;
        self.currentIndex = self.toIndex;
    }];

}


- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated
{
    if (selectedIndex == self.currentIndex) { return; }
    _totalCount = 0;
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberPagesInHZWPagerContentView:)]) {
        _totalCount = [self.dataSource numberPagesInHZWPagerContentView:self];
    }
    
    if (selectedIndex > _totalCount - 1) {  return; }
    self.toIndex = selectedIndex;
    
    // 获取即将展示的VC
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(hzwPagerContentView:viewControllerForIndex:)]) {
        self.toViewController = [self.dataSource hzwPagerContentView:self viewControllerForIndex:self.toIndex];
    }
    
    if (!self.toViewController) { return; }
    if (self.toViewController.parentViewController) { return; }
    
    if (self.currentViewController) {
        [self.currentViewController willMoveToParentViewController:nil];
        [self.currentViewController beginAppearanceTransition:NO animated:animated];
    }
    
    [self.parentViewController addChildViewController:self.toViewController];
//    [self.toViewController beginAppearanceTransition:YES animated:YES];
    [self.toViewController willMoveToParentViewController:self.parentViewController];
    [self addSubview:self.toViewController.view];
    
    BOOL isNextPage = self.currentIndex < self.toIndex;
    
    if (isNextPage) {
        self.toViewController.view.frame = CGRectMake(self.bounds.size.width, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    }
    else {
        self.toViewController.view.frame = CGRectMake(-self.bounds.size.width, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    }
    
    NSTimeInterval animateDuration = animated ? kHZWAnimateDuration : 0.0;
    
    if (animated) {
        [UIView animateWithDuration:animateDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            if (isNextPage) {
                self.currentViewController.view.frame = CGRectMake(-CGRectGetWidth(self.bounds), self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
            }
            else {
                self.currentViewController.view.frame = CGRectMake(CGRectGetWidth(self.bounds), self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
            }
            self.toViewController.view.frame = CGRectMake(0, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        } completion:^(BOOL finished) {
            if (!finished) { return; }
            if (self.currentViewController) {
                [self.currentViewController.view removeFromSuperview];
                [self.currentViewController endAppearanceTransition];
                [self.currentViewController removeFromParentViewController];
            }
            [self.toViewController didMoveToParentViewController:self.parentViewController];
            //        [self.toViewController endAppearanceTransition];
            
            self.currentViewController = self.toViewController;
            self.currentIndex = self.toIndex;
            
            // Delegate
            if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerContentView:didSuccessToNextPage:)]) {
                [self.delegate hzwPagerContentView:self didSuccessToNextPage:self.currentIndex];
            }
        }];
    }
    else {
        if (isNextPage) {
            self.currentViewController.view.frame = CGRectMake(-CGRectGetWidth(self.bounds), self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        }
        else {
            self.currentViewController.view.frame = CGRectMake(CGRectGetWidth(self.bounds), self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        }
        self.toViewController.view.frame = CGRectMake(0, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
        
        if (self.currentViewController) {
            [self.currentViewController.view removeFromSuperview];
            [self.currentViewController endAppearanceTransition];
            [self.currentViewController removeFromParentViewController];
        }
        [self.toViewController didMoveToParentViewController:self.parentViewController];
        //        [self.toViewController endAppearanceTransition];
        
        self.currentViewController = self.toViewController;
        self.currentIndex = self.toIndex;
        
        // Delegate
        if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerContentView:didSuccessToNextPage:)]) {
            [self.delegate hzwPagerContentView:self didSuccessToNextPage:self.currentIndex];
        }
    }
    
}

- (void)reloadData
{
    _totalCount = 0;
    if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberPagesInHZWPagerContentView:)]) {
        _totalCount = [self.dataSource numberPagesInHZWPagerContentView:self];
    }
    
    if (_totalCount > 0) {
        [self setSelectedIndex:0 animated:YES];
    }
}

#pragma mark - gestures
- (void)handPanGesture:(UIPanGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:self];
    // 如果手势刚开始，那么记录开始位置
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        // 重置状态
        self.startPanLocation = location;
        self.toIndex = -1;
        self.toViewController = nil;
        
        _totalCount = 0;
        if (self.dataSource && [self.dataSource respondsToSelector:@selector(numberPagesInHZWPagerContentView:)]) {
            _totalCount = [self.dataSource numberPagesInHZWPagerContentView:self];
        }
        
//        // 当前的UIViewController发出ViewWillDisappear信息
//        [self.currentViewController beginAppearanceTransition:NO animated:YES];
    }
    // 如果手势属于变化状态当中，那么开始滑动页面
    else if (recognizer.state == UIGestureRecognizerStateChanged) {
        CGFloat offsetX = location.x - self.startPanLocation.x;
        
        if (offsetX < 0) {
            if (self.toIndex != self.currentIndex + 1) {
                // 当前的UIViewController发出ViewWillDisappear信息
                [self.currentViewController beginAppearanceTransition:NO animated:YES];
            }
            self.toIndex = self.currentIndex + 1;
        }
        else {
            if (self.toIndex != self.currentIndex - 1) {
                // 当前的UIViewController发出ViewWillDisappear信息
                [self.currentViewController beginAppearanceTransition:NO animated:YES];
            }
            self.toIndex = self.currentIndex - 1;
        };
        
        if (self.toIndex >= 0 && self.toIndex < _totalCount) {
            // 设置VC页面滑动位置，模拟UIScrollView滑动效果
            // 1.获取新的VC
            // 防止重复获取
            if (!self.toViewController) {
                if (self.dataSource && [self.dataSource respondsToSelector:@selector(hzwPagerContentView:viewControllerForIndex:)]) {
                    self.toViewController = [self.dataSource hzwPagerContentView:self viewControllerForIndex:self.toIndex];
                }
                // 如果当前VC没有加入
                if (self.toViewController && !self.toViewController.parentViewController) {
                    [self.parentViewController addChildViewController:self.toViewController];
                    [self.toViewController willMoveToParentViewController:self.parentViewController];
                    [self.toViewController beginAppearanceTransition:YES animated:YES];
                    [self addSubview:self.toViewController.view];
                }
            }
            
            // 设置移动位置
            [self configureChildViewFrameWithOffset:offsetX];
        }
        // 新页面超出索引边界，减缓边缘移动距离
        else {
            // 设置移动位置
            [self configureChildViewFrameWithOffset:offsetX / 3.0];
        }
    }
    // 手势结束，判定是到下一页还是回到当前页
    else if (recognizer.state == UIGestureRecognizerStateEnded) {
        CGFloat offsetX = location.x - self.startPanLocation.x;
        // 移动
        if (self.toIndex >= 0 && self.toIndex < _totalCount && self.toIndex != self.currentIndex) {
            // 如果移动距离超过阈值，那么进入下一页
            if (fabs(offsetX) > kHZWOffsetThreshold) {
                [UIView animateWithDuration:kHZWAnimateDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    [self configureChildViewFrameWithOffset:offsetX > 0 ? self.bounds.size.width : -self.bounds.size.width];
                } completion:^(BOOL finished) {
                    if (!finished) { return; }
                    // 移除当前VC
                    [self.currentViewController willMoveToParentViewController:nil];
                    [self.currentViewController.view removeFromSuperview];
                    [self.currentViewController endAppearanceTransition];
                    [self.currentViewController removeFromParentViewController];
                    // 展现下个VC
                    [self.toViewController endAppearanceTransition];
                    [self.toViewController didMoveToParentViewController:self.parentViewController];
                    
                    self.currentIndex = self.toIndex;
                    self.currentViewController = self.toViewController;
                    
                    // Delegate
                    if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerContentView:didSuccessToNextPage:)]) {
                        [self.delegate hzwPagerContentView:self didSuccessToNextPage:self.currentIndex];
                    }
                }];
            }
            // 如果移动距离未超过阈值，那么回到当前页
            else {
                [self resetChildViewFrame];
            }
        }
        else {
            [self resetChildViewFrame];
        }
    }
    else {
        [self resetChildViewFrame];
    }
}

#pragma mark - private methods
- (void)resetChildViewFrame
{
    [UIView animateWithDuration:kHZWAnimateDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self configureChildViewFrameWithOffset:0.f];
    } completion:^(BOOL finished) {
        if (!finished) { return; }
        
        if (!self.toViewController) { return; }
        [self.currentViewController beginAppearanceTransition:YES animated:YES];
        // 移除VC
        [self.toViewController willMoveToParentViewController:nil];
        
        [self.toViewController beginAppearanceTransition:NO animated:YES];
        [self.toViewController.view removeFromSuperview];
        [self.toViewController endAppearanceTransition];
        [self.toViewController removeFromParentViewController];
        self.toViewController = nil;
        
        [self.currentViewController endAppearanceTransition];
        
        // Delegate
        if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerContentView:didCancelToNextPage:)]) {
            [self.delegate hzwPagerContentView:self didCancelToNextPage:self.currentIndex];
        }
    }];
}

- (void)configureChildViewFrameWithOffset:(CGFloat)offsetX
{
    CGFloat delta = 0;
    if (self.toIndex < self.currentIndex) {
        delta = self.bounds.origin.x - self.bounds.size.width + offsetX;
    }
    else {
        delta = self.bounds.origin.x + self.bounds.size.width + offsetX;
    }
    self.currentViewController.view.frame = CGRectMake(self.bounds.origin.x + offsetX, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    if (self.toViewController) {
        self.toViewController.view.frame = CGRectMake(delta, self.bounds.origin.y, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds));
    }
    
    // Delegate
    if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerContentView:fromIndex:toIndex:percent:)]) {
        [self.delegate hzwPagerContentView:self fromIndex:self.currentIndex toIndex:self.toIndex percent:fabs(offsetX) / self.bounds.size.width];
    }
}


#pragma mark - setters
- (void)setScrollEnabled:(BOOL)scrollEnabled
{
    _scrollEnabled = scrollEnabled;
    self.panGestureRecognizer.enabled = scrollEnabled;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    if (_currentIndex != selectedIndex) {
        // 刷新当前VC
        [self setSelectedIndex:selectedIndex animated:NO];
    }
}
#pragma mark - getters
- (NSInteger)selectedIndex
{
    return _currentIndex;
}

- (UIPanGestureRecognizer *)panGestureRecognizer
{
    if (!_panGestureRecognizer) {
        _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handPanGesture:)];
    }
    return _panGestureRecognizer;
}

- (UIViewController *)parentViewController
{
    return [self viewController];
}

@end

NS_ASSUME_NONNULL_END
