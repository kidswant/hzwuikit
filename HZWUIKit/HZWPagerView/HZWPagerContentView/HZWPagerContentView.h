//
//  HZWPagerContentView.h
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 Leisure. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HZWPagerContentView;

@protocol HZWPagerContentViewDataSource <NSObject>

/**
 *  返回一共多少页
 *
 *  @param pagerContentView HZWPagerContentView
 *
 *  @return 总VC页面数量
 */
- (NSInteger)numberPagesInHZWPagerContentView:(HZWPagerContentView *)pagerContentView;

/**
 *  请求对应索引的Index
 *
 *  @param pagerContentView HZWPagerContentView
 *  @param index            对应的索引
 *
 *  @return UIViewController
 */
- (__kindof UIViewController *)hzwPagerContentView:(HZWPagerContentView *)pagerContentView viewControllerForIndex:(NSInteger)index;

@end

@protocol HZWPagerContentViewDelegate <NSObject>

@optional
/**
 *  切换页面过程中的状态
 *
 *  @param pagerContentView HZWPagerContentView
 *  @param fromIndex        原VC索引
 *  @param toIndex          现VC索引
 *  @param percent          页面offset百分比
 */
- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex percent:(CGFloat)percent;

/**
 *  成功切换到下一个页面
 *
 *  @param pagerContentView HZWPagerContentView
 *  @param toIndex          下一个页面的索引
 */
- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView didSuccessToNextPage:(NSInteger)toIndex;

/**
 *  回到当前页面
 *
 *  @param pagerContentView HZWPagerContentView
 *  @param fromIndex        当前页面的索引
 */
- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView didCancelToNextPage:(NSInteger)fromIndex;

@end


@interface HZWPagerContentView : UIView

@property (nonatomic, weak) id<HZWPagerContentViewDataSource> dataSource;

@property (nonatomic, weak) id<HZWPagerContentViewDelegate> delegate;

/**
 *  当前展现的VC的index
 */
@property (nonatomic, assign) NSInteger selectedIndex;

/**
 *  是否可以滑动
 */
@property (nonatomic, assign) BOOL scrollEnabled;

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated;

- (void)changeSelectedIndex:(NSInteger)selectedIndex;

- (void)reloadData;

@end

NS_ASSUME_NONNULL_END



/**
 *  ToDo: 待修复在切换页面动画过程中，又进行一次页面切换
 *  Warning: "Unbalanced calls to begin/end appearance transitions"
 */
