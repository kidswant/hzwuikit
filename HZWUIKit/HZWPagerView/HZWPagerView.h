//
//  HZWPagerView.h
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HZWPagerView;


@interface HZWPagerTabItem : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIFont *font;

@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) UIColor *selectedTitleColor;

@end

@protocol HZWPagerViewDataSource <NSObject>

-(NSInteger)numberOfPagesInHZWPagerView:(HZWPagerView *)pagerView;

- (__kindof UIViewController *)hzwPagerView:(HZWPagerView *)pagerView viewControllerForIndex:(NSInteger)index;

@end

@protocol HZWPagerViewDelegate <NSObject>

@optional
- (void)hzwPagerView:(HZWPagerView *)pagerView didSelectedAtIndex:(NSInteger)index;

@end

@interface HZWPagerView : UIView

@property (nonatomic, weak) id<HZWPagerViewDataSource> dataSource;
@property (nonatomic, weak) id<HZWPagerViewDelegate> delegate;

@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, strong) NSArray<HZWPagerTabItem *> *items;

@property (nonatomic, assign) CGFloat tabBarHeight;

@property (nonatomic, assign) BOOL scrollEnabled;

/**
 *  Title颜色渐变
 */
@property (nonatomic, assign) BOOL enabledMixColor;
@property (nonatomic, strong) UIColor *trackColor;

@property (nonatomic, assign) BOOL animated;

@end
