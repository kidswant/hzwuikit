//
//  HZWPagerView.m
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "HZWPagerView.h"
#import "HZWPagerContentView.h"
#import "HZWPagerFixedTabView.h"

@interface HZWPagerView ()<HZWPagerContentViewDataSource, HZWPagerContentViewDelegate, HZWPagerTabViewDelegate>

@property (nonatomic, strong) HZWPagerContentView *pagerContentView;
@property (nonatomic, strong) HZWPagerFixedTabView *tabView;

@end

@implementation HZWPagerView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [self addSubview:self.tabView];
    [self addSubview:self.pagerContentView];
    self.tabBarHeight = 40;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.tabView.frame = CGRectMake(0, 0, self.bounds.size.width, self.tabBarHeight);
    self.pagerContentView.frame = CGRectMake(0, self.tabBarHeight, self.bounds.size.width, self.bounds.size.height - self.tabBarHeight);
}

#pragma mark - HZWPagerContentViewDataSource
- (NSInteger)numberPagesInHZWPagerContentView:(HZWPagerContentView *)pagerContentView
{
    return [self.dataSource numberOfPagesInHZWPagerView:self];
}

- (__kindof UIViewController *)hzwPagerContentView:(HZWPagerContentView *)pagerContentView viewControllerForIndex:(NSInteger)index
{
    return [self.dataSource hzwPagerView:self viewControllerForIndex:index];
}

#pragma mark - HZWPagerContentViewDelegate
- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView didSuccessToNextPage:(NSInteger)toIndex
{
    [self.tabView setSelectedIndex:toIndex animated:self.tabView.trackAnimated];
    if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerView:didSelectedAtIndex:)]) {
        [self.delegate hzwPagerView:self didSelectedAtIndex:toIndex];
    }
}

- (void)hzwPagerContentView:(HZWPagerContentView *)pagerContentView fromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex percent:(CGFloat)percent
{
    [self.tabView changeTabFromIndex:fromIndex toIndex:toIndex percent:percent];
}

#pragma mark - HZWPagerTabViewDelegate
- (void)hzwPagerTabView:(HZWPagerFixedTabView *)pagerTabView didSelectedAtIndex:(NSInteger)index
{
    [self.pagerContentView setSelectedIndex:index animated:self.animated];
}

#pragma mark - getters and setter
- (HZWPagerContentView *)pagerContentView
{
    if (!_pagerContentView) {
        _pagerContentView = [[HZWPagerContentView alloc] init];
        _pagerContentView.dataSource = self;
        _pagerContentView.delegate = self;
        _pagerContentView.scrollEnabled = YES;
    }
    
    return _pagerContentView;
}

- (HZWPagerFixedTabView *)tabView
{
    if (!_tabView) {
        _tabView = [[HZWPagerFixedTabView alloc] init];
        _tabView.delegate = self;
        _tabView.enabledMixColor = YES;
    }
    
    return _tabView;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    _selectedIndex = selectedIndex;
//    [self.tabView setSelectedIndex:_selectedIndex animated:self.animated];
    [self.tabView changeSelectedIndex:_selectedIndex];
    [self.pagerContentView changeSelectedIndex:_selectedIndex];
//    [self.pagerContentView setSelectedIndex:_selectedIndex animated:self.animated];
}

- (void)setItems:(NSArray<HZWPagerTabItem *> *)items
{
    self.tabView.items = items;
}

- (void)setTrackColor:(UIColor *)trackColor
{
    self.tabView.trackColor = trackColor;
}

- (void)setScrollEnabled:(BOOL)scrollEnabled
{
    self.pagerContentView.scrollEnabled = scrollEnabled;
}

@end
