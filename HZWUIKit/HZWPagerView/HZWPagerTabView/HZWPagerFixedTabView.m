//
//  HZWPagerFixedTabView.m
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import "HZWPagerFixedTabView.h"
#import "UIView+HZWLayout.h"
#import "UIColor+HZW.h"
#import "HZWPagerTabItem.h"
#import "NSString+HZW.h"

static const NSTimeInterval kHZWAnimateDuration = .25f;

@interface HZWPagerFixedTabView () {
    CGFloat _trackHeight;
}

@property (nonatomic, strong) UIImageView *backgroundView;
@property (nonatomic, strong) UIImageView *trackView;
@property (nonatomic, strong) NSMutableArray<UILabel *> *labelList;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@end

@implementation HZWPagerFixedTabView

- (instancetype)initWithItems:(NSArray<HZWPagerTabItem *> *)items innerMargin:(CGFloat)innerMargin
{
    // 计算文本内容
    CGFloat width = 0.0f;
    for (HZWPagerTabItem *item in items) {
        width += [item.title widthForFont:item.font];
    }
    width += innerMargin * items.count;
    
    if (self = [self initWithFrame:CGRectMake(0, 0, width, 0)]) {
        self.items = items;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    [self addSubview:self.backgroundView];
    [self addSubview:self.trackView];
    
    [self addGestureRecognizer:self.tapGestureRecognizer];
    self.trackHeight = 1.0;
    self.trackAnimated = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.backgroundView.frame = self.bounds;
    [self layoutTabItems];
}

- (void)layoutTabItems
{
    if (self.labelList.count == 0) { return; };
    CGFloat width = self.width / self.labelList.count;
    CGFloat height = self.height;
    CGFloat offsetX = 0.0f;
    for (NSInteger index = 0; index < self.labelList.count; index++) {
        UILabel *label = self.labelList[index];
        if (_selectedIndex == index) {
            HZWPagerTabItem *currentItem = [self.items objectAtIndex:_selectedIndex];
            label.textColor = currentItem.selectedTitleColor;
        }
        label.frame = CGRectMake(offsetX, 0, width, height);
        offsetX += width;
    }
    
    // trackView frame
    self.trackView.frame = CGRectMake(_selectedIndex * width, self.height - self.trackHeight, width, self.trackHeight);
}

#pragma mark - public methods
- (void)changeSelectedIndex:(NSInteger)selectedIndex
{
    if (_selectedIndex == selectedIndex) { return; }
    // 恢复当前的Label状态
    if (_selectedIndex >= 0 && _selectedIndex < self.items.count) {
        UILabel *currentLabel = [self.labelList objectAtIndex:_selectedIndex];
        HZWPagerTabItem *currentItem = [self.items objectAtIndex:_selectedIndex];
        currentLabel.textColor = currentItem.titleColor;
        
        // 更新即将展现的Label状态
        UILabel *toLabel = [self.labelList objectAtIndex:selectedIndex];
        HZWPagerTabItem *toItem = [self.items objectAtIndex:selectedIndex];
        toLabel.textColor = toItem.selectedTitleColor;
        
        self.trackView.originX = self.width / self.items.count * selectedIndex;
        
        _selectedIndex = selectedIndex;
    }

}


- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated
{
    if (_selectedIndex == selectedIndex) { return; }
    // 恢复当前的Label状态
    if (_selectedIndex >= 0 && _selectedIndex < self.items.count) {
        UILabel *currentLabel = [self.labelList objectAtIndex:_selectedIndex];
        HZWPagerTabItem *currentItem = [self.items objectAtIndex:_selectedIndex];
        currentLabel.textColor = currentItem.titleColor;
        
        // 更新即将展现的Label状态
        UILabel *toLabel = [self.labelList objectAtIndex:selectedIndex];
        HZWPagerTabItem *toItem = [self.items objectAtIndex:selectedIndex];
        toLabel.textColor = toItem.selectedTitleColor;
        
        if (animated) {
            self.tapGestureRecognizer.enabled = self.enabledGestureWhenAnimating;
            [UIView animateWithDuration:kHZWAnimateDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.trackView.originX = self.width / self.items.count * selectedIndex;
            } completion:^(BOOL finished) {
                self.tapGestureRecognizer.enabled = YES;
            }];
        } else {
            self.trackView.originX = self.width / self.items.count * selectedIndex;
        }
        
        _selectedIndex = selectedIndex;
        // Delegate
        if (self.delegate && [self.delegate respondsToSelector:@selector(hzwPagerTabView:didSelectedAtIndex:)]) {
            [self.delegate hzwPagerTabView:self didSelectedAtIndex:_selectedIndex];
        }
    }
}

- (void)changeTabFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex percent:(CGFloat)percent
{
    if (fromIndex < 0 || toIndex < 0 || fromIndex >= self.items.count || toIndex >= self.items.count) { return; }
    UILabel *currentLabel = [self.labelList objectAtIndex:fromIndex];
    UILabel *toLabel = [self.labelList objectAtIndex:toIndex];
    HZWPagerTabItem *currentItem = [self.items objectAtIndex:fromIndex];
    HZWPagerTabItem *toItem = [self.items objectAtIndex:toIndex];
    if (self.enabledMixColor) {
        currentLabel.textColor = [currentItem.titleColor mixColorWithDestinationColor:currentItem.selectedTitleColor percent:percent];
        toLabel.textColor = [toItem.selectedTitleColor mixColorWithDestinationColor:toItem.titleColor percent:percent];
    }
    
    CGFloat width = self.width / self.items.count;
    CGFloat trackOffsetX = 0.0f;
    if (fromIndex < toIndex) {
        trackOffsetX = width * fromIndex + width * percent;
    } else {
        trackOffsetX = width * fromIndex - width * percent;
    }
    
    self.trackView.originX = trackOffsetX;
}

#pragma mark - private methods
- (void)createItemViews
{
    [self removeAllLabels];
    if (self.items.count == 0) { return; }
    _selectedIndex = 0;
    CGFloat width = self.width / self.items.count;
    CGFloat height = self.height;
    for (NSInteger index = 0; index < self.items.count; index++) {
        HZWPagerTabItem *item = [self.items objectAtIndex:index];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        label.text = item.title;
        label.backgroundColor = self.backgroundColor;
        label.textColor = item.titleColor;
        label.highlightedTextColor = item.selectedTitleColor;
        label.textAlignment = NSTextAlignmentCenter;
        if (item.font) {
            label.font = item.font;
        }
        else {
            label.font = [UIFont systemFontOfSize:15.0];
        }
        
        [self.labelList addObject:label];
        [self addSubview:label];
    }
    [self setNeedsLayout];
}

- (void)removeAllLabels
{
    if (self.labelList.count > 0) {
        for (UILabel *label in self.labelList) {
            [label removeFromSuperview];
        }
        
        [self.labelList removeAllObjects];
    }
}

#pragma mark - gesture
- (void)handleTapGesture:(UITapGestureRecognizer *)recognizer
{
    if (self.items.count == 0) { return; }
    CGPoint location = [recognizer locationInView:self];
    
    // Gesture点击事件会在超出frame的范围之内接受事件
    if (!CGRectContainsPoint(recognizer.view.bounds, location)) {
        return;
    }
    CGFloat width = self.width / self.items.count;
    [self setSelectedIndex:location.x / width animated:self.trackAnimated];
}

#pragma mark - getters and setters

- (NSMutableArray<UILabel *> *)labelList
{
    if (!_labelList) {
        _labelList = [[NSMutableArray<UILabel *> alloc] init];
    }
    
    return _labelList;
}

- (UIImageView *)backgroundView
{
    if (!_backgroundView) {
        _backgroundView = [[UIImageView alloc] init];
    }
    return _backgroundView;
}

- (UIImageView *)trackView
{
    if (!_trackView) {
        _trackView = [[UIImageView alloc] init];
    }
    return _trackView;
}

- (void)setTrackHeight:(CGFloat)trackHeight
{
    _trackHeight = trackHeight;
    [self setNeedsLayout];
}

- (void)setItems:(NSArray<HZWPagerTabItem *> *)items
{
    if (_items != items) {
        _items = items;
        // 设置View
        [self createItemViews];
    }
}

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    self.backgroundView.image = backgroundImage;
}

- (void)setTrackColor:(UIColor *)trackColor
{
    self.trackView.backgroundColor = trackColor;
}

- (UITapGestureRecognizer *)tapGestureRecognizer
{
    if (!_tapGestureRecognizer) {
        _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    }
    
    return _tapGestureRecognizer;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex
{
    [self setSelectedIndex:_selectedIndex animated:NO];
}

@end
