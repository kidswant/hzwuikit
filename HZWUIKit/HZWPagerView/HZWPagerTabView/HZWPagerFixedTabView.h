//
//  HZWPagerFixedTabView.h
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HZWPagerTabViewProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@class HZWPagerFixedTabView;
@class HZWPagerTabItem;

@interface HZWPagerFixedTabView : UIView

@property (nonatomic, strong) NSArray<HZWPagerTabItem *> *items;
@property (nonatomic, strong) UIColor *trackColor;
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, assign) NSInteger selectedIndex;
@property (nonatomic, assign) CGFloat trackHeight;

@property (nonatomic, assign) BOOL enabledMixColor;
@property (nonatomic, assign) BOOL trackAnimated;

// tab 之间的间距
@property (nonatomic, weak) id<HZWPagerTabViewDelegate> delegate;

/**
 *  动画过程中屏蔽点击事件
 */
@property (nonatomic, assign) BOOL enabledGestureWhenAnimating;

- (void)setSelectedIndex:(NSInteger)selectedIndex animated:(BOOL)animated;

- (void)changeTabFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex percent:(CGFloat)percent;

- (void)changeSelectedIndex:(NSInteger)selectedIndex;

- (instancetype)initWithItems:(NSArray<HZWPagerTabItem *> *)items innerMargin:(CGFloat)innerMargin;

@end

NS_ASSUME_NONNULL_END