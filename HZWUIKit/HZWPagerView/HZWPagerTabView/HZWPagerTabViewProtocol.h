//
//  HZWPagerTabViewProtocol.h
//  HZWUIKit
//
//  Created by ZhangMing on 4/5/16.
//  Copyright © 2016 ZhangMing. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HZWPagerFixedTabView;

@protocol HZWPagerTabViewDelegate <NSObject>

- (void)hzwPagerTabView:(HZWPagerFixedTabView *)pagerTabView didSelectedAtIndex:(NSInteger)index;

@end
