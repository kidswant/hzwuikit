//
//  UIView+Kidswant.m
//  KidswantKit
//
//  Created by ZhangMing on 3/15/16.
//  Copyright © 2016 Aaron Zhang. All rights reserved.
//

#import "UIView+HZW.h"

@implementation UIView (HZW)

- (void)stayIntrinsicSize {
    [self stayIntrinsicWidth];
    [self stayIntrinsicHeight];
}

- (void)stayIntrinsicWidth {
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
}

- (void)stayIntrinsicHeight {
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    [self setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
}

- (CGSize)compressedSize {
    return [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize withHorizontalFittingPriority:UILayoutPriorityDefaultHigh verticalFittingPriority:UILayoutPriorityDefaultHigh];
}

- (UIImage *)snapshotImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

- (void)removeAllSubviews {
    while (self.subviews.count > 0) {
        [self.subviews.lastObject removeFromSuperview];
    }
}

- (UIViewController *)viewController {
    UIView *view = self;
    do {
        UIResponder *nextResponder = [view nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)nextResponder;
        }
        view = view.superview;
    } while (view != nil);
    
    return nil;
}

@end
