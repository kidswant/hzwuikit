//
//  UIView+Kidswant.h
//  KidswantKit
//
//  Created by ZhangMing on 3/15/16.
//  Copyright © 2016 Aaron Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (HZW)


- (void)stayIntrinsicSize;

- (void)stayIntrinsicWidth;

- (void)stayIntrinsicHeight;

- (CGSize)compressedSize;

- (UIImage *)snapshotImage;

- (void)removeAllSubviews;

- (UIViewController *)viewController;


@end
