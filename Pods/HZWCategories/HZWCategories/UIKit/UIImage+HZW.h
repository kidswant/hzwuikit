//
//  UIImage+Kidswant.h
//  KidswantKit
//
//  Created by Kidswant on 3/4/16.
//  Copyright © 2016 Kidswant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (HZW)

- (UIImage *)imageByCropToRect:(CGRect)rect;

- (UIImage *)imageByRoundCornerRadius:(CGFloat)radius;

- (UIImage *)imageByRoundCornerRadius:(CGFloat)radius
                          borderWidth:(CGFloat)borderWidth
                          borderColor:(UIColor *)borderColor;

- (UIImage *)imageByRoundCornerRadius:(CGFloat)radius
                              corners:(UIRectCorner)corners
                          borderWidth:(CGFloat)borderWidth
                          borderColor:(UIColor *)borderColor
                       borderLineJoin:(CGLineJoin)borderLineJoin;

@end
