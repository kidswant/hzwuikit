//
//  UIControl+Kidswant.h
//  KidswantKit
//
//  Created by ZhangMing on 3/15/16.
//  Copyright © 2016 Aaron Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIControl (HZW)

- (void)removeAllTargets;

- (void)setTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;

- (void)addBlockForControlEvents:(UIControlEvents)controlEvents block:(void (^)(id sender))block;

- (void)setBlockForControlEvents:(UIControlEvents)controlEvents block:(void (^)(id sender))block;

- (void)removeAllBlocksForControlEvents:(UIControlEvents)controlEvents;

@end
